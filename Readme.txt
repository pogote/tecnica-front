Front:

- Dirigirse a la siguiente url https://bitbucket.org/pogote/tecnica-front/src/master/
- Dar clic en boton Clone que se encuentra en la esquina superior derecha.
- En el popup que se abrirá seleccionar clonar en sourcetree (tener en cuenta que para usar esta opción, es necesario tener el sourcetree descargado con anterioridad).
- Cuando se abra el sourcetree, seleccionar la carpeta en la cual queremos clonar el proyecto subido.
- Finalmente abrir el visual studio code (Tenerlo descargado con anterioridad).
- Seleccionar archivo, abrir carpeta y seleccionar la carpeta donde guardamos el front.
- Abrir la consola del visual studio code y colocar el siguiente comando (ng serve, este comando abrirá el front en el puerto 4200), en caso que queramos cambiar de puerto
para levantar el front, debemos usar el mismo comango ng-serve --port (puerto deseado).