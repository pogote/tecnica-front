import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { Article, ResponseNewsViewModel } from 'src/app/models/article';
import { ServiceNewsService } from 'src/app/services/service-news.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from "@angular/material/snack-bar";
import { Pais } from 'src/app/models/pais';

@Component({
  selector: 'app-ultimas-noticias',
  templateUrl: './ultimas-noticias.component.html',
  styleUrls: ['./ultimas-noticias.component.scss']
})
export class UltimasNoticiasComponent implements OnInit {
  @ViewChild(MatPaginator) paginator?:MatPaginator;
  obs!: Observable<any>;
  dataSource: any;
  country: string = 'ar';
  page: any = null;
  pageSize: any = null;
  disabledUI = true;
  lstPaises: Array<Pais> = new Array<Pais>();
  selectedPais!: string;
  constructor(private changeDetectorRef: ChangeDetectorRef,
              private snackBar: MatSnackBar, 
              private _noticiasService: ServiceNewsService) { }

  ngOnInit(): void {
    this.ultimasNoticias(this.country);
    this.getPaises();
  }

  getPaises() {
    var pais1 = new Pais();
    var pais2 = new Pais();
    var pais3 = new Pais();

    pais1.nombre = 'Argentina';
    pais1.value = 'arg';

    pais2.nombre = 'Colombia',
    pais2.value = 'co';

    pais3.nombre = 'Francia';
    pais3.value = 'fr';

    this.lstPaises.push(pais1);
    this.lstPaises.push(pais2);
    this.lstPaises.push(pais3);

    this.selectedPais = this.lstPaises[0].value;
  }

  ultimasNoticias(country: string) {
    this._noticiasService.TopHeadlines(country).subscribe((resp: ResponseNewsViewModel) => {
      if(resp.status == 'ok') {
        this.dataSource = new MatTableDataSource<Article>(resp.articles);
        this.dataSource.paginator = this.paginator;
        this.changeDetectorRef.detectChanges();
        this.obs = this.dataSource.connect();
        this.disabledUI = false;
      }else {
        this.snackBar.open(resp.message, 'Ok', {
          duration: 3000
        })
      }
    },error => {
      this.snackBar.open('Error de servicio', 'Ok', {
        duration: 3000
      });
    });
  }

  goNoticia(urlNoticia: string) {
    window.open(urlNoticia, "_blank");
  }
}

