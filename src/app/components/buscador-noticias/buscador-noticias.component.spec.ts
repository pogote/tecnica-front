import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscadorNoticiasComponent } from './buscador-noticias.component';

describe('BuscadorNoticiasComponent', () => {
  let component: BuscadorNoticiasComponent;
  let fixture: ComponentFixture<BuscadorNoticiasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuscadorNoticiasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscadorNoticiasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
