import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { ResponseNewsViewModel } from 'src/app/models/article';
import { ServiceNewsService } from 'src/app/services/service-news.service';
import { DatePipe } from '@angular/common';
import { Pais } from 'src/app/models/pais';


@Component({
  selector: 'app-buscador-noticias',
  templateUrl: './buscador-noticias.component.html',
  styleUrls: ['./buscador-noticias.component.scss']
})
export class BuscadorNoticiasComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator,  { static: false }) paginator?:MatPaginator;
  obs!: Observable<any>;
  dataSource:any;
  disabledUI = true;
  fechaDesde: any;
  fechaHasta: any;
  keywords: any;
  form: any;
  minDate: any;
  maxDate: any;
  hoy: any;
  mostrarPaginator: boolean = false;
  pageSize: Array<number> = new Array<number>();
  length: number = 0;
  lstPaises: Array<Pais> = new Array<Pais>();
  selectedPais!: string;
  
  constructor(private changeDetectorRef: ChangeDetectorRef,
              private datePipe: DatePipe,
              private snackBar: MatSnackBar,
              private _noticiasService: ServiceNewsService) { }

  ngOnInit(): void {
    this.hoy = new Date();
    this.getPaises();
  }

  ngOnDestroy() {
    if (this.dataSource) { 
      this.dataSource.disconnect(); 
    }
  }

  getPaises() {
    this.disabledUI = true;
    var pais1 = new Pais();
    var pais2 = new Pais();
    var pais3 = new Pais();

    pais1.nombre = 'Argentina';
    pais1.value = 'arg';

    pais2.nombre = 'Colombia',
    pais2.value = 'co';

    pais3.nombre = 'Francia';
    pais3.value = 'fr';

    this.lstPaises.push(pais1);
    this.lstPaises.push(pais2);
    this.lstPaises.push(pais3);

    this.selectedPais = this.lstPaises[0].value;
    this.disabledUI = false;
  }

  Search() {
    if(this.validateFullField()) {
      this.fechaDesde = this.datePipe.transform(this.fechaDesde, 'yyyy-MM-dd');
      this.fechaHasta = this.datePipe.transform(this.fechaHasta, 'yyyy-MM-dd');
      this._noticiasService.Search(this.fechaDesde, this.fechaHasta, this.keywords, this.selectedPais).subscribe((resp: ResponseNewsViewModel) => {
        if(resp.status == 'ok') {
          this.dataSource = new MatTableDataSource<any>(resp.articles);
          this.dataSource.paginator = this.paginator;
          this.changeDetectorRef.detectChanges();
          this.obs = this.dataSource.connect();
          this.disabledUI = false;
        }else {
          this.snackBar.open(resp.message, 'Ok', {
            duration: 3000
          })
        }
      },error => {
         this.snackBar.open('Error de servicio', 'Ok', {
           duration: 3000
         });
      });
    }else {
      this.snackBar.open('Debe completar todos los campos antes de realizar la búsqueda', 'Ok', {
        duration: 3000
      });
    }
  }

  validateFullField() {
    if((this.fechaDesde != undefined || this.fechaDesde != null) && (this.fechaHasta != undefined || this.fechaHasta != null)) {
      if(this.keywords == undefined || this.keywords == null || this.keywords == '') {
        return false;
      }
      else {
        return true;
      }
     }else {
       return false;
     }
  }

  setMaxDate(event: any) {
    this.maxDate = event.value;
  }

  goNoticia(urlNoticia: string) {
    window.open(urlNoticia, "_blank");
  }
}
