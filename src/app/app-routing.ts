import { RouterModule, Routes } from '@angular/router';
import { BuscadorNoticiasComponent } from './components/buscador-noticias/buscador-noticias.component';
import { UltimasNoticiasComponent } from './components/ultimas-noticias/ultimas-noticias.component';

const appRoutes = [
    { path: '', component: UltimasNoticiasComponent},
    { path: 'ultimasNoticias', component: UltimasNoticiasComponent},
    { path: 'buscadorNoticias', component: BuscadorNoticiasComponent}
  ];

export const routing = RouterModule.forRoot(appRoutes);