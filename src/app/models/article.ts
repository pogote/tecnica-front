export class Article {
    source: Source;
    author!: string;
    title!: string;
    description!: string;
    url!: string;
    urlToImage!: string;
    publishedAt!: string;
    content!: string;

    constructor() {
        this.source = new Source();
    }
}

export class Source {
    id!: string;
    name!: string;
}

export class ResponseNewsViewModel {
    status!: string;
    totalResults!: number;
    articles: Array<Article>;
    message!: string;
    pageSize!: number;
    page!: number;

    constructor() {
        this.articles = new Array<Article>();
    }
}
