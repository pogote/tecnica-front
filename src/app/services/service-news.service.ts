import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ResponseNewsViewModel } from '../models/article';

@Injectable({
  providedIn: 'root'
})
export class ServiceNewsService {
  
  private url = `${environment.apiUrl}/news`;
  constructor(private httpClient: HttpClient) { }

  public TopHeadlines(country: string): Observable<ResponseNewsViewModel> {
    return this.httpClient.get<ResponseNewsViewModel>(`${this.url}/top-headlines/${country}`);
  }

  public Search(dateFrom: Date, dateTo: Date, keywords: string, country: string): Observable<ResponseNewsViewModel> {
    return this.httpClient.get<ResponseNewsViewModel>(`${this.url}/search/${dateFrom}/${dateTo}/${keywords}/${country}`);
  }
}
